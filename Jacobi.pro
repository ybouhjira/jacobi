#-------------------------------------------------
#
# Project created by QtCreator 2012-12-26T21:02:30
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Jacobi
TEMPLATE = app


SOURCES += main.cpp \
    MainWindow.cpp \
    MatrixModel.cpp

HEADERS  += \
    MainWindow.h \
    Matrice.h \
    MatrixModel.h

RESOURCES += \
    ressources.qrc
