#include "MainWindow.h"
#include <QAction>
#include <QToolBar>
#include <QSpinBox>
#include <QTableView>
#include <QVBoxLayout>
#include <QSplitter>
#include <QLabel>
#include <QInputDialog>
#include <QStatusBar>
#include "MatrixModel.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , m_toolBar(new QToolBar("Barre d'outils"))
    , m_changeAct(new QAction("Changer", this))
    , m_solveAct(new QAction("Résoudre l'équation",this))
    , m_spinBox(new QSpinBox)
    , m_aView(new QTableView)
    , m_bView(new QTableView)
    , m_xView(new QTableView)
    , m_aModel(new MatrixModel(Matrice<double>(5,5)))
    , m_bModel(new MatrixModel(Matrice<double>(5,1)))
    , m_xModel(new MatrixModel(Matrice<double>(5,1)))
{
    setWindowTitle("Résolution d'équation à l'aide de la méthode Jacobi");
    QStatusBar *statusBar = new QStatusBar;
    setStatusBar(statusBar);
    statusBar->showMessage("Youssef Bouhjira - 2012 - EST d'Essaouira");

    // Barre d'outils
    m_toolBar->setMovable(false);
    addToolBar(m_toolBar);
    m_toolBar->addWidget(new QLabel("nombre de lignes : "));
    m_toolBar->addWidget(m_spinBox);
    m_toolBar->addAction(m_changeAct);
    m_toolBar->addSeparator();
    m_toolBar->addAction(m_solveAct);

    // Spinbox
    m_spinBox->setMinimum(2);
    m_spinBox->setMaximum(100);

    // Zone central de la fenetre
    QSplitter *splitter = new QSplitter(Qt::Horizontal);
    setCentralWidget(splitter);
    QWidget *aWidget = new QWidget();
    QWidget *bWidget = new QWidget();
    QWidget *xWidget = new QWidget();
    QVBoxLayout *aLayout = new QVBoxLayout(aWidget);
    QVBoxLayout *bLayout = new QVBoxLayout(bWidget);
    QVBoxLayout *xLayout = new QVBoxLayout(xWidget);
    aLayout->addWidget(new QLabel("Matrice A"));
    bLayout->addWidget(new QLabel("Vecteur b"));
    xLayout->addWidget(new QLabel("Solution X"));
    aLayout->addWidget(m_aView);
    bLayout->addWidget(m_bView);
    xLayout->addWidget(m_xView);
    splitter->addWidget(aWidget);
    splitter->addWidget(bWidget);
    splitter->addWidget(xWidget);

    // Models et vues
    m_aView->setModel(m_aModel);
    m_bView->setModel(m_bModel);
    m_xView->setModel(m_xModel);
    // desactiver l'édition dur le tableau de X
    m_xView->setEditTriggers(QAbstractItemView::NoEditTriggers);

    connect(m_changeAct, SIGNAL(triggered()), this, SLOT(changeLineCount()) );
    connect(m_solveAct, SIGNAL(triggered()), this, SLOT(solve()));
}

void MainWindow::changeLineCount(){
    int count = m_spinBox->value();
    m_aModel->resizeMatrix(count , count);
    m_bModel->resizeMatrix(count , 1);
    m_xModel->resizeMatrix(count, 1);
}

void MainWindow::solve() {
    // On récupére la matrice A et b
    Matrice<double> A = m_aModel->getMatrix();
    Matrice<double> b = m_bModel->getMatrix();

    // On calcule E F et D^-1
    Matrice<double> D_1 = A.getDiagonal().inverseVal();
    Matrice<double> E = A.getLower() * -1;
    Matrice<double> F = A.getUpper() * -1;

    // On calcule Bj et bj
    Matrice<double> Bj = D_1 * ( E + F );
    Matrice<double> bj = D_1 * b ;

    // Itérations
    Matrice<double> Xk(b.getLines(), 1);
    Xk.init(1); //< X0 = (1, 1, 1, ...)
    int nbrIter = QInputDialog::getInt(this,"","Entrez le nombre d'itérations");
    for (int i=0; i<nbrIter; i++)
        Xk = Bj * Xk + bj ;

    // Affichage du résultat:
    m_xModel->setMatrix(Xk);
}
