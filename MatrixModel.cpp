#include "MatrixModel.h"

MatrixModel::MatrixModel(Matrice<double> m, QObject *parent) :
    QAbstractTableModel(parent)
  , m_matrice(m)
{
}

void MatrixModel::resizeMatrix(int lines, int columns){
    Matrice<double> newMatrix(lines, columns) ;
    beginResetModel();
    for(int i=0; i<lines; i++){
        for(int j=0; j<columns; j++){
            if(i >= m_matrice.getLines() || j >= m_matrice.getColumns() )
                newMatrix(i,j) = 0;
            else
                newMatrix(i,j) = m_matrice(i,j);
        }

    }
    m_matrice = newMatrix;
    endResetModel();
}

int MatrixModel::rowCount(const QModelIndex &) const{
    return m_matrice.getLines();
}

int MatrixModel::columnCount(const QModelIndex &) const{
    return m_matrice.getColumns();
}

QVariant MatrixModel::data
(const QModelIndex &index, int role = Qt::DisplayRole ) const{
    if (!index.isValid() || (role != Qt::DisplayRole && role != Qt::EditRole) )
        return QVariant();
    return m_matrice(index.row(), index.column());
}

bool MatrixModel::setData
(const QModelIndex &index, const QVariant &value, int){
    bool ok;
    double number = value.toDouble(&ok);

    // Si la conversion à été faite
    if(ok){
        m_matrice(index.row(), index.column()) = number ;
        return true;
    }

    // Sinon en retourne false
    return false;
}

Qt::ItemFlags MatrixModel::flags(const QModelIndex &) const{
    return
            Qt::ItemIsEditable |
            Qt::ItemIsEnabled |
            Qt::ItemIsSelectable |
            Qt::ItemIsDragEnabled |
            Qt::ItemIsDropEnabled |
            Qt::ItemIsUserCheckable ;
}

Matrice<double> MatrixModel::getMatrix(){
    return m_matrice;
}

void MatrixModel::setMatrix(const Matrice<double> &m){
    beginResetModel();
    m_matrice = m ;
    endResetModel();
}
