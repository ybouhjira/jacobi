#ifndef MATRICE_H
#define MATRICE_H

#include <vector>

/**
 * @brief Class de matrices
 */
template<class T> class Matrice
{
// METHODES
public:
    /**
     * @brief Constructeur
     * @param lines : nombre de lignes
     * @param columns : nombre de colonnes
     */
    Matrice(int lines, int columns){
        m_lines = lines ;
        m_columns = columns ;

        for(int i=0; i<lines ; i++){
            m_data.push_back(std::vector<T>(columns));
        }
    }

    /**
     * @brief Initialise tout les valeurs de la matrice par value
     */
    void init(T const& value){
        for(int i=0; i<m_lines; i++)
            for(int j=0; j<m_columns; j++)
                m_data[i][j] = value;
    }

    /**
     * @brief renvoie une matrice dont les valeurs sont inverse
     * @return Matrice
     */
    Matrice inverseVal() const{
        Matrice resultat(m_lines, m_columns);
        for(int i=0; i<m_lines; i++){
            for(int j=0; j<m_columns; j++){
                if( i == j )
                    resultat(i,j) = 1/ m_data[i][j] ;
                else
                    resultat(i,j) = 0;
            }
        }

        return resultat ;
    }

    /**
     * @brief Cette méthode permet d'acceder a l'elemnt Aij
     * @param i : ligne
     * @param j : colonne
     * @return retourne une reference sur l'element
     */
    T& operator()(int i, int j) {
        return m_data[i][j];
    }

    /**
     * @brief voir la méthode précédante
     */
    T operator()(int i, int j) const {
        return m_data[i][j];
    }

    /**
     * @brief Surcharge de l'opérateur + pour faire l'addition de deux matrices
     * @param m : une autre matrice
     * @return La matrice résultat
     */
    Matrice operator+(Matrice const &m) const {
        Matrice<T> resultat(m_lines, m_columns);
        for(int i=0; i<m_lines; i++)
            for(int j=0; j<m_columns; j++)
                resultat(i,j) = m(i,j) + m_data[i][j] ;
        return resultat ;
    }

    /**
     * @brief Surcharge de l'operateur * pour faire la multiplication de deux
     * matrices
     * @param m : une autre matrice
     * @return La matrice résultat
     */
    Matrice operator*(Matrice const &m) const {
        Matrice<T> resultat(m_lines, m.m_columns);

        //calcul
        for(int i=0; i<m_lines; i++)
            for(int j=0; j<m.m_columns; j++)
                for(int x=0; x<m_columns; x++)
                    resultat(i,j) += m_data[i][x] * m(x,j);
        return resultat ;
    }

    /**
     * @brief fait le produit de value avec la matrice
     * @param value : un nombre
     * @return la matrice résultat
     */
    Matrice operator *( T const& value) const{
        Matrice resultat(m_lines, m_columns);

        for(int i=0; i<m_lines; i++)
            for(int j=0; j<m_columns; j++)
                resultat(i,j) = m_data[i][j] * value ;
        return resultat ;
    }

    // Methodes Jacobi
    /**
     * @brief
     * @return
     */
    Matrice getDiagonal(){
        Matrice resultat(m_lines, m_columns);
        for(int i=0; i<m_lines; i++){
            for(int j=0; j<m_columns; j++){
                if(i == j)
                    resultat(i,j) = m_data[i][j];
                else
                    resultat(i,j) = 0 ;
            }
        }
        return resultat ;
    }

    /**
     * @brief getupper
     * @return
     */
    Matrice getUpper(){
        Matrice resultat(m_lines, m_columns);
        for(int i=0; i<m_lines; i++){
            for(int j=0; j<m_columns; j++){
                if(i < j)
                    resultat(i,j) = m_data[i][j];
                else
                    resultat(i,j) = 0 ;
            }
        }
        return resultat ;
    }

    /**
     * @brief getlower
     * @return
     */
    Matrice getLower(){
        Matrice resultat(m_lines, m_columns);
        for(int i=0; i<m_lines; i++){
            for(int j=0; j<m_columns; j++){
                if(i > j)
                    resultat(i,j) = m_data[i][j];
                else
                    resultat(i,j) = 0 ;
            }
        }
        return resultat ;
    }

    /**
     * @return nombre de lignes
     */
    int getLines() const{
        return m_lines ;
    }

    /**
     * @return nombre de colonnes
     */
    int getColumns() const{
        return m_columns ;
    }

// ATTRIBUTS
private:
    int m_lines, m_columns;
    std::vector< std::vector<T> > m_data;
};

template<class T>
Matrice<T> operator*(T const& value, Matrice<T> const& m){
    return m * value ;
}

#endif // MATRICE_H
