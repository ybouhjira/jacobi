#ifndef MATRIXMODEL_H
#define MATRIXMODEL_H

#include <QAbstractTableModel>
#include "Matrice.h"

class MatrixModel : public QAbstractTableModel
{
    Q_OBJECT
public:
    /**
     * @brief Constructeur
     * @param parent : object parent
     */
    explicit MatrixModel(Matrice<double> m,QObject *parent = 0);

    /**
     * @return la matrice contenue dans ce modéle
     */
    Matrice<double> getMatrix();

    /**
     * @brief change la matrice
     * @param nouvelle matrice
     */
    void setMatrix(Matrice<double> const& m);

    /**
     * @brief Redimensionne la matrice
     * @param lines : nombre de lignes
     * @param columns : nombre de colonnes
     */
    void resizeMatrix(int lines, int columns);

    // Reimplementer les methodes abstraites
    /**
     * @return le nombre de lignes
     */
    int rowCount(const QModelIndex &) const;

    /**
     * @return Le nombre de colonnes
     */
    int columnCount(const QModelIndex &) const;

    /**
     * @param index : represente l'indice d'un element dans le model
     * @return la valeur dans la case qui a l'indice index
     */
    QVariant data(const QModelIndex &index, int role) const;

    /**
     * @brief setData permet d'éditer les données à partir de la table
     * @param index : indice de la case
     * @param value : valeur
     */
    bool setData(const QModelIndex &index, const QVariant &value, int);

    /**
     * @brief Indique si l'element est editable
     * @param index : indice de l'element
     * @return true
     */
    Qt::ItemFlags flags(const QModelIndex &) const;

private:
    Matrice<double> m_matrice;
};

#endif // MATRIXMODEL_H
