#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
class QSpinBox;
class QTableView;
class MatrixModel;

class MainWindow : public QMainWindow
{
    Q_OBJECT

// METHODES
public:
    MainWindow(QWidget *parent = 0);

private slots:
    /**
     * @brief Change le nombre de lignes
     */
    void changeLineCount();

    /**
     * @brief resoud l'eqution
     */
    void solve();

// ATTRIBUTS
private:

    // Widgets
    QToolBar *m_toolBar;
    QAction *m_changeAct, *m_solveAct;
    QSpinBox *m_spinBox;
    QTableView *m_aView, *m_bView, *m_xView;

    // Model
    MatrixModel *m_aModel, *m_bModel, *m_xModel;
};

#endif // MAINWINDOW_H
